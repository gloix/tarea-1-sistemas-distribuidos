package pongd;

import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.Set;

public interface IServer extends Remote {

	String getIp() throws RemoteException;
	public IPong setPong(IPong pong) throws RemoteException;
	public IPong getPong() throws RemoteException;
	public Set<IServer> agregarServidor(IServer serverNuevo) throws RemoteException;
	public void registrarEnRed(IServer server, IServer vecino) throws RemoteException;
	public double getCarga() throws RemoteException;
	public IServer serverMinCarga(Set<IServer> servers) throws RemoteException;
	public IPlayer getPlayer() throws RemoteException;
	public IPlayer getPlayer(int i) throws RemoteException;
}
