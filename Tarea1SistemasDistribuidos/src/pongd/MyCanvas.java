package pongd;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JPanel;

public class MyCanvas extends JPanel {
	private static final int W_RANKING = 250, H_RANKING = 200;
	private static final int W_PAUSED = 250, H_PAUSED = 70;
	public static final int PAUSE_NONE = 0, PAUSE_PAUSED = 1, PAUSE_REQUESTED = 2;

	private static final long serialVersionUID = 2915397754067560840L;

	public List<Rectangle> rectangles = new ArrayList<Rectangle>();
	public List<Integer> scores;
	public Object lock = new Object();
	public boolean mostrarRanking = false;
	public int pausedState = 0;
	public boolean mostrarPresioneEnter = false;

	public MyCanvas() {
		scores = new ArrayList<Integer>();
	}

	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, getWidth(), getHeight());
		g.setColor(Color.WHITE);
		synchronized (lock) {
			for (Rectangle rectangle : rectangles) {
				rectangle.draw(g);
			}
			int fontHeight = g.getFontMetrics().getHeight();
			for (int i = 0; i < scores.size(); i++) {
				g.drawString("P" + (i + 1) + ": " + scores.get(i), 50, 50 + i * fontHeight);
			}

			if (pausedState != PAUSE_NONE) {
				Graphics g_ = g.create();
				g_.translate(getWidth() / 2 - W_PAUSED / 2, getHeight() / 2 - H_PAUSED / 2);
				if (pausedState == PAUSE_REQUESTED) {
					g_.drawString("PAUSED", 10, 10);
				} else {
					g_.drawString("GAME PAUSED BY OTHER PLAYER", 10, 10);
				}
			}

			if (mostrarRanking) {
				Graphics g_ = g.create();
				g_.translate(getWidth() / 2 - W_RANKING / 2, getHeight() / 2 - H_RANKING / 2);
				g_.setColor(new Color(1, 1, 1, 0.3f));
				g_.fillRect(0, 0, W_RANKING, H_RANKING);
				g_.setColor(Color.WHITE);
				g_.drawRect(0, 0, W_RANKING, H_RANKING);
				//List<Integer> orderedScoresList = new ArrayList<Integer>();
				List<Integer> scoresAux = new ArrayList<Integer>(scores);
				for(int i=0; i< scoresAux.size();i++) {
					scoresAux.set(i, (scoresAux.get(i)+1)*1000+i);
				}
				Collections.sort(scoresAux);
				Collections.reverse(scoresAux);
				Font defaultFont = g_.getFont();
				for (int i = scoresAux.size()-1; i >= 0; i--) {
					if (i == 0) {
						g_.setFont(g_.getFont().deriveFont(
								(float) (g_.getFontMetrics().getHeight() + 10)));
					}
					int posJugador = scoresAux.get(i)%1000;
					int puntaje = (scoresAux.get(i)-0)/1000-1;
					g_.drawString(
							"Player "
									+ (posJugador + 1)
									+ ": "
									+ puntaje, 10, 25 + i * fontHeight);
					g_.setFont(defaultFont);
					
				}
				if(mostrarPresioneEnter)
					g_.drawString("<Press Enter to start new game>", 10, H_RANKING - fontHeight
							- 10);

			}

		}

	}
}
