package pongd;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IPlayer extends Remote {
	public boolean isUp() throws RemoteException;
	public boolean isDown() throws RemoteException;
	public boolean isActive() throws RemoteException;
	public void setUp(boolean up) throws RemoteException;
	public void setDown(boolean down) throws RemoteException;
	public void setPauseRequested(boolean requested) throws RemoteException;
	public boolean isPauseRequested() throws RemoteException;
	public void setScore(int score) throws RemoteException;
	public int getScore() throws RemoteException;
	public IClient getClient() throws RemoteException;
	public void setClient(IClient iclient) throws RemoteException;
	public int getPlayerNumber() throws RemoteException;
	public void migrate(IPong p) throws RemoteException;
	void togglePause() throws RemoteException;
}


