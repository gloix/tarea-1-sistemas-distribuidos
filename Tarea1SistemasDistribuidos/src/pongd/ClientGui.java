package pongd;

import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.Arrays;

import javax.swing.JFrame;

public class ClientGui implements KeyListener {

	public final static String TITLE = "Pong - CC5303";
	public final static int WIDTH = 640, HEIGHT = 480;
	public final static int UPDATE_RATE = 60;
	public final static int DX = 5;
	public final static double DV = 0.1;

	public JFrame frame;
	public MyCanvas canvas;
	IPong pong;
	IPlayer me;

	private boolean couldNotUpdate = false;

	public ClientGui() {
		// Inicializamos canvas al principio para poder darle referencias de
		// bars en cualquier momento.
		// Incluso antes de llamar a init.
		canvas = new MyCanvas();
	}

	public void setIPong(IPong pong) {
		System.out.println("Cliente: Pong seteado");
		this.pong = pong;
	}

	public void setIPlayer(IPlayer player) {
		System.out.println("Cliente: Player seteado");
		this.me = player;
	}

	public void init() {
		System.out.println("init ClientGui");
		frame = new JFrame(TITLE);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		canvas.setSize(WIDTH, HEIGHT);
		canvas.setPreferredSize(new Dimension(WIDTH, HEIGHT));

		frame.add(canvas);
		frame.pack();
		frame.addKeyListener(this);
		// Thread dedicado a actualizar el estado del juego
		Thread game = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {
						if (pong != null) {
							synchronized (canvas.lock) {
								
								canvas.rectangles = new ArrayList<Rectangle>(Arrays.asList(pong
										.getBars()));
								couldNotUpdate = false;
								canvas.rectangles.add(pong.getBall());
								int state = pong.getState();
								canvas.scores = pong.getScores();
								if(state == Pong.S_GAMEFINISHED) {
									canvas.mostrarRanking = true;
									if(!pong.isPlayerReadyOnFinish(me.getPlayerNumber())) {
										canvas.mostrarPresioneEnter = true;
									} else {
										canvas.mostrarPresioneEnter = false;
									}
									//canvas.mostrarPresioneEnter = true;
								} else {
									canvas.mostrarRanking = false;
								}
								if(state == Pong.S_PAUSED) {
									/*if(me.isPauseRequested()) {
										canvas.pausedState = MyCanvas.PAUSE_REQUESTED;
									} else {*/
										canvas.pausedState = MyCanvas.PAUSE_PAUSED;
									//}
								} else {
									canvas.pausedState = MyCanvas.PAUSE_NONE;
								}
							}
						}
						Thread.sleep(50);
					} catch (RemoteException e) {
						couldNotUpdate = true;
						System.exit(0);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (NullPointerException e) {
					}
				}
			}
		});

		// Thread dedicado a actualizar el canvas local
		Thread canvasUpdate = new Thread(new Runnable() {
			@Override
			public void run() {
				while(true) {
					canvas.repaint();
					try {
						if(pong == null) {
							frame.setTitle("Pong - Conectando...");
						} else if (couldNotUpdate) {
							frame.setTitle("Pong - Conexión perdida...");
						} else if(pong.getErrorState() == Pong.ES_PLAYERDISCONNECTED) {
							frame.setTitle("Pong - Un jugador se desconectó.");
						} else {
							frame.setTitle("Pong - Conectando");
						}
					} catch (RemoteException e1) {
						couldNotUpdate = true;
						System.out.println("Server error. You can restart if you connect to server with --num-player=<num you got at beginning>.");
						System.exit(0);
					}
					try {
						Thread.sleep(1000 / UPDATE_RATE); // milliseconds
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		});
		canvasUpdate.start();

		game.start();
	}

	@Override
	public void keyPressed(KeyEvent event) {
		if (me != null) {
			int keyCode = event.getKeyCode();
			switch (keyCode) {
			case KeyEvent.VK_UP:
				try {
					me.setUp(true);
				} catch (RemoteException e) {
				}
				break;
			case KeyEvent.VK_DOWN:
				try {
					me.setDown(true);
				} catch (RemoteException e) {
				}
				break;
			case KeyEvent.VK_P:
				try {
					me.togglePause();
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				break;
			case KeyEvent.VK_ENTER:
				try {
					if(pong.getState() == Pong.S_GAMEFINISHED)
						pong.setPlayerReadyOnFinish(me.getPlayerNumber());
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				break;
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent event) {
		if (me != null) {
			int keyCode = event.getKeyCode();
			switch (keyCode) {
			case KeyEvent.VK_UP:
				try {
					me.setUp(false);
				} catch (RemoteException e) {
				}
				break;
			case KeyEvent.VK_DOWN:
				try {
					me.setDown(false);
				} catch (RemoteException e) {
				}
				break;
			}
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {
	}
	
}
