package pongd;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

public class Client extends UnicastRemoteObject implements IClient {
	private static final long serialVersionUID = -1587531220549942195L;
	
	private IPong pong;
	private IServer server;
	
	ClientGui cg;
	List<IPlayer> players;
	
	public Client() throws RemoteException {
		cg = new ClientGui();
		players = new ArrayList<IPlayer>();
	}
	
	public void start(String ip, int numPlayer) {
		System.out.println("starting game...");
		String url = "rmi://" + ip + ":" + "1099" + "/";
		cg.init();
		IPlayer player = null;
		try {
			this.server = (IServer)Naming.lookup(url + "server");
			if (numPlayer >= 0) player = this.server.getPlayer(numPlayer);
			else player = this.server.getPlayer();
			player.setClient(this);
			this.players.add(player);
			System.out.println("Player "+player.getPlayerNumber()+" activated. Use this number to reconnect if client error.");
			cg.setIPlayer(player);
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		IPong pong = null;
		
		try {
			pong = (IPong) Naming.lookup(url + "pong");
			cg.setIPong(pong);
			this.pong=pong;
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (NotBoundException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
	}
	
	// Argumentos: --num-player={num|false}, --server=ip del servidor
	static public void main(String[] args) throws Exception {
		int numPlayer = -1;
		if (!args[0].substring(args[0].indexOf('=')+1).equals("false")) try {
			numPlayer = Integer.parseInt(args[0].substring(args[0].indexOf('=')+1));
		} catch (NumberFormatException e) {e.printStackTrace();}
		String serverIp = args[1].substring(args[1].indexOf('=')+1);
		Client c = new Client();
		c.start(serverIp, numPlayer);
	}

	@Override
	public boolean isAlive() throws RemoteException {
		return true;
	}

	public void migrate(IPong p, int playerNumber) throws RemoteException { //merece refactoring xD
		IPlayer player = null;
		cg.setIPong(null);
		try {
			//player = (IPlayer)Naming.lookup(url + "player_" + playerNumber);
			player = p.getPlayers()[playerNumber];
			System.out.println("found player slot!");
			player.setClient(this);
			/* setear score del nuevo player con el score del viejo player */
			for (IPlayer pe : this.players) {
				if (pe.getPlayerNumber() == playerNumber) {
					player.setScore(pe.getScore());
					players.remove(pe);
					players.add(player);
				}
			}
			System.out.println("player " + playerNumber + " activated");
			cg.setIPlayer(player);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		IPong pong = null;
		System.out.println("Migrating");
		System.out.println("finding pong");
		pong = p;
		System.out.println("found pong");
		cg.setIPong(pong);
		this.pong=p;
	}

	@Override
	public void togglePause() throws RemoteException {
		int pongState = pong.getState();
		if (pongState == Pong.S_PAUSED)
			pong.setState(Pong.S_RUNNING);
		else if(pongState == Pong.S_RUNNING)
			pong.setState(Pong.S_PAUSED);
	}
}
