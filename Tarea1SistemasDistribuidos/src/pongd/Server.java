package pongd;

import java.io.File;
import java.net.MalformedURLException;
import java.rmi.ConnectException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

import profiler.cpuData;

public class Server extends UnicastRemoteObject implements IServer{
	private static final long serialVersionUID = -6317278553362733375L;
	
	private static final int DEFAULT_NUM_PLAYERS = 2;
	private String url;
	private String ip;
	private IPong pong;
	private Set<IServer> vecinos;
	
	public Server(String ip) throws RemoteException {
		this.ip = ip;
		this.url = "rmi://" + ip + ":1099/";
		this.vecinos= new HashSet<IServer>();
	}
	
	public Server(String file, String ip) throws RemoteException {
		this(ip);
		File f = new File(file);
		pong = new Pong(f);
	}
	
	public Server(String file, String ip, String ipVecino) throws RemoteException, MalformedURLException, NotBoundException  {
		this(ip);
		File f = new File(file);
		String urlVecino = "rmi://" + ipVecino + ":1099/server";
		IServer vecino = (IServer)Naming.lookup(urlVecino); 
		vecinos.add(vecino);
		System.out.println("Registrando este servidor en la red...");
		registrarEnRed(this, vecino);
		pong = new Pong(f);
	}
	
	public Server(int numPlayers, String ip, String ipVecino) throws RemoteException, MalformedURLException, NotBoundException {
		this(ip);
		String urlVecino = "rmi://" + ipVecino + ":1099/server";
		IServer vecino = (IServer)Naming.lookup(urlVecino); 
		vecinos.add(vecino);
		System.out.println("Registrando este servidor en la red...");
		registrarEnRed(this, vecino);
		pong = new Pong(numPlayers);
	}
	
	public Server(int nbPlayers, String ip) throws RemoteException, MalformedURLException, NotBoundException {
		this(ip);
		pong = new Pong(nbPlayers);
	}

	public void registrarEnRed(IServer server, IServer vecino) {
		Set<IServer> otrosVecinos = new HashSet<IServer>();
		try {
			otrosVecinos = vecino.agregarServidor(server);
			System.out.println("Lista de vecinos obtenidos "+otrosVecinos.size()+" "+otrosVecinos.toString());
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		vecinos.addAll(otrosVecinos);
	}
	
	public Set<IServer> agregarServidor(IServer serverNuevo) {
		System.out.println("Agregando servidor");
		if (!vecinos.add(serverNuevo)) return new HashSet<IServer>();
		else {
			Set<IServer> vecinosSobrevivientes = new HashSet<IServer>();
			for (IServer vecino : vecinos) {
				try {
					vecino.agregarServidor(serverNuevo);
					System.out.println("Servidor agregado!");
					vecinosSobrevivientes.add(vecino);
				} catch (ConnectException e) {
					System.out.println("Se deconectó un servidor");
				} catch (RemoteException e) {
					System.out.println("Jiústön, we got a problem!");
					e.printStackTrace();
				}
			}
			vecinos = vecinosSobrevivientes;
		}
		System.out.println(vecinos);
		return vecinos;
	}
	
	public void start() {
		try {
			Naming.rebind(url + "server", this);
			Naming.rebind(url + "pong", pong);
			System.out.println("Pong published. Waiting for players");
		} catch (RemoteException e) {
			System.out.println("No connection");
			e.printStackTrace();
			return;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		wait:
			while (true) {
				try {
					boolean allPlayersActive = true;
					for(int i=0;i<this.pong.getPlayers().length;i++){
						if(!this.pong.getPlayers()[i].isActive()){
							allPlayersActive = false;
						}
					}
					if (allPlayersActive) break wait;
					//Thread.sleep(100);
				} catch (Exception e) {e.printStackTrace();}
			}
		
		// A PARTIR DE ESTA LINEA LOS JUGADORES YA ESTAN CONECTADOS AL SERVIDOR
		
		try {
			System.out.println("Starting game with " + this.pong.getPlayers().length + " players.");
		} catch(Exception e) {e.printStackTrace();}
		
		((Pong)pong).setState(Pong.S_PAUSED);
		
		System.out.println("Players connected");
		System.out.println("Starting");
		try {
			pong.init();
		} catch (RemoteException e) {e.printStackTrace();}
		new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					int aliveCount = 0;
					IPlayer iplayer = null;
					try {
						for(int i=0;i<pong.getPlayers().length;i++) {
							iplayer = pong.getPlayers()[i];
							IClient iclient = iplayer.getClient();
							if(iclient != null) {
								iclient.isAlive();
								aliveCount++;
							}
						}
						if(aliveCount == pong.getPlayers().length) ((Pong)pong).setErrorState(Pong.ES_NOERRORS);
						Thread.sleep(100);
					} catch (InterruptedException e) {
						e.printStackTrace();
					} catch (RemoteException e) {
						System.out.println("Client disconnected! Game will go on without it.");
						((Pong)pong).setErrorState(Pong.ES_PLAYERDISCONNECTED);
						((Player)iplayer).setClient(null);
					}
				}
			}
			
		}).start();
		new Thread(new Runnable() {
			
			@Override
			public void run() {
				while(true) {
					try {
						pong.saveGameState(new File("game.sav"));
						Thread.sleep(500);
					} catch (RemoteException e) {
						System.out.println(true);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			}
		}).start();
	}
	
	public boolean migrate() throws RemoteException {
		//synchronized(vecinos) {
			IServer vecino = serverMinCarga(vecinos);
			if(vecino == null) {
				return false;
			}
			try {
				//CAMBIAR ESTO HACE QUE SE REINICIE EL JUEGO CUANDO MIGRA
				IPong p = vecino.setPong(this.pong); 
				for (IPlayer player : this.pong.getPlayers()) {
					if(!player.isActive()) continue;
					try {
						player.getClient().migrate(p, player.getPlayerNumber());
					} catch(RemoteException e) {
						System.out.println("No se pudo migrar un cliente");
					}
				}
				
			} catch(RemoteException e) {
				///////////////////////////////////////////////e.printStackTrace();
			}
			return true;
		//}
	}
	
	public IPong setPong(IPong pong) throws RemoteException {
		this.pong = new Pong(pong);
		return this.pong;
	}

	public String getIp() throws RemoteException {
		return ip;
	}
	
	// Argumentos: --load-state={true|path de archivo}, --players=numero jugadores, --my-ip= ip propia --another-ip={false|otra ip}
	static public void main(String[] args) {
		int nbPlayers = DEFAULT_NUM_PLAYERS; //Numero de jugadores
		String fileState = null; //Direccion de archivo para cargar o false
		String myIp; //Mi ip en red
		String neighborIp; //Ip de otro servidor o false
		
		if (args[0].substring(args[0].indexOf('=')+1).equals("false")) fileState = null;
		else fileState = args[0].substring(args[0].indexOf('=')+1);
		try {
			nbPlayers = Integer.parseInt(args[1].substring(args[1].indexOf('=')+1));
		}
		catch (NumberFormatException e) {
			System.err.println("Error. Using" + DEFAULT_NUM_PLAYERS+ " as number of players");
		}	
		myIp = args[2].substring(args[2].indexOf('=')+1);
		if (args[3].substring(args[3].indexOf('=')+1).equals("false")) neighborIp = null;
		else neighborIp = args[3].substring(args[3].indexOf('=')+1);
		
		Server s = null;
		try {
			if (fileState != null) {
				if (neighborIp != null) s = new Server(fileState, myIp, neighborIp);
				else s = new Server(fileState, myIp);
			}
			else {
				if (neighborIp != null) s = new Server(nbPlayers, myIp, neighborIp);
				else s = new Server(nbPlayers, myIp);
			}
		} catch(RemoteException e) {
			e.printStackTrace();System.exit(-1);
		} catch (MalformedURLException e) {
			e.printStackTrace();System.exit(-1);
		} catch (NotBoundException e) {
			e.printStackTrace();System.exit(-1);
		}
		
		s.start();
		System.out.println("Esperando línea");
		Scanner sc = new Scanner(System.in);
		String command = sc.nextLine();
		System.out.println("Línea leída" + command);
		try {
			if(s.migrate())
				System.out.println("He migrado.");
			else
				System.out.println("No he migrado. Forever alone.");
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		
//		new Thread(new Runnable() {
//			Server server;
//			Runnable setServer(Server server) {
//				this.server=server;
//				return this;
//			}
//			@Override
//			public void run() {
//					while(true){
//						double load = cpuData.getCpuUsage();
//						if (load >0.70){
//							try {
//								server.migrate();
//								System.out.println("He migrado.");
//							} catch (RemoteException e) {
//								e.printStackTrace();
//							}
//							break;
//						}
//						try {
//							Thread.sleep(100);
//						} catch (InterruptedException e) {
//							// TODO Auto-generated catch block
//							e.printStackTrace();
//						}
//					}
//			}
//		}.setServer(s)).start();
	}
	
	public IPong getPong() throws RemoteException {
		return this.pong;
	}
	
	public double getCarga() throws RemoteException {
		return cpuData.getCpuUsage();
	}
	
	public IServer serverMinCarga(Set<IServer> servers) {
		double min = Double.MAX_VALUE;
		IServer resultado = null;
		List<IServer> porEliminar = new ArrayList<IServer>();
		for(IServer s : servers){
			try{
				if(s.getIp().equals(this.getIp())) continue;
				double cargaServer= s.getCarga();
				if(cargaServer < min){
					min = cargaServer;
					resultado = s;
				}
			} catch(RemoteException e){
				porEliminar.add(s);
				System.err.println("No respondió un vecino");
			}
		}
		servers.removeAll(porEliminar);
		return resultado;
	}
	
	public IPlayer getPlayer() throws RemoteException {
		for (IPlayer p: this.pong.getPlayers()) {
			if (!p.isActive()) {
				return p;
			}
		}
		return null;
	}
	
	public IPlayer getPlayer(int i) throws RemoteException {
		return this.pong.getPlayers()[i];
	}
}

