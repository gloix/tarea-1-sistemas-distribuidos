package pongd;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Player extends UnicastRemoteObject implements IPlayer {

	boolean up;
	boolean down;
	private boolean pauseRequested; 
	private int score;
	private IClient client;
	private final int playerNumber;
	

	@Override
	public int getScore() throws RemoteException{
		return score;
	}

	@Override
	public void setScore(int score) throws RemoteException{
		this.score = score;
	}
	
	@Override
	public IClient getClient() {
		return this.client;
	}
	
	@Override
	public void setClient(IClient iclient) {
		this.client = iclient;
	}

	private static final long serialVersionUID = -1993004842051840918L;
	
	public Player(int playerNumber) throws RemoteException{
		this.playerNumber = playerNumber;
		up = false;
		down = false;
		score = 0;
		pauseRequested = true;
		this.client = null;
	}
	
	@Override
	public boolean isUp() {
		return up;
	}
	
	@Override
	public boolean isDown() {
		return down;
	}
	
	@Override
	public synchronized boolean isActive() {
		return this.client != null;
	}
	
	@Override
	public void setUp(boolean up) {
		this.up = up;
	}
	
	@Override
	public void setDown(boolean down) {
		this.down = down;
	}
	
	@Override
	public void setPauseRequested(boolean requested) throws RemoteException {
		this.pauseRequested = requested; 
	}
	
	@Override
	public boolean isPauseRequested() throws RemoteException {
		return pauseRequested;
	}
	
	@Override
	public int getPlayerNumber () {
		return playerNumber;
	}
	
	public void migrate(IPong p) throws RemoteException{
		this.client.migrate(p, this.playerNumber);
	}
	
	@Override
	public void togglePause() throws RemoteException {
		this.client.togglePause();
	}}


