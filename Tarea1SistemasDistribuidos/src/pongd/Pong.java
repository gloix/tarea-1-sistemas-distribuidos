package pongd;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.List;

public class Pong extends UnicastRemoteObject implements IPong {
	private static final long serialVersionUID = 8902582799081367859L;
	
	
	public final static int WIDTH = 640, HEIGHT = 480;
	public final static int UPDATE_RATE = 60;
	public final static int DX = 5;
	public final static double DV = 0.1;
	public final static double initialSpeed = 0.8;
	
	
	public Rectangle bars[];
	public IPlayer players[];
	
	public IPlayer[] getPlayers() throws RemoteException{
		return players;
	}


	public Rectangle ball;
	public static final int S_RUNNING = 0, S_PAUSED = 1, S_WAITINGFORPLAYERS = 2, S_GAMEFINISHED = 3;
	public static final int ES_NOERRORS = 0, ES_PLAYERDISCONNECTED = 1;
	private int errorState;
	private int state;
	private int maxPoints;
	private boolean[] playersReadyOnFinish;
	private int nbLastPlayerThatHitTheBall; //mas informacion en http://www.allroundautomations.com/images/pls100longident.png
	
	
	@Override
	public synchronized int getState() throws RemoteException {
		return this.state;
	}

	public synchronized void setState(int state) {
		this.state = state;
	}
	
	@Override
	public synchronized int getErrorState() throws RemoteException{
		return this.errorState;
	}

	public synchronized void setErrorState(int state) {
		this.errorState = state;
	}
	
	private double vx, vy;

	public Pong(int numPlayers) throws RemoteException {
		playersReadyOnFinish = new boolean[numPlayers];
		setState(S_WAITINGFORPLAYERS);
		this.players = new IPlayer[numPlayers];
		for (int i = 0; i < numPlayers; i++) {
			players[i] = new Player(i);
			playersReadyOnFinish[i] = false;
		}
		this.maxPoints = 10;
		//Llamamos a setupNewGame con players seteado
		setupNewGame();
	}
	
	public Pong(IPong pong) throws RemoteException {
		this.players = new IPlayer[pong.getPlayerSlots().length];
		for (int i = 0; i < this.players.length; i++) {
			players[i] = new Player(i);
		}
		this.bars = pong.getBars();
		this.ball = pong.getBall();
		this.state = pong.getState();
		this.errorState = pong.getErrorState();
		this.vx = pong.getVx();
		this.vy = pong.getVy();
		this.playersReadyOnFinish = pong.getPlayersReadyOnFinish();
		this.nbLastPlayerThatHitTheBall = pong.getNbLastPlayerThatHitTheBall();
		this.maxPoints = pong.getMaxPoints();
	}
	
	public Pong(File file) throws RemoteException {
		BufferedReader reader = null;
		try{
			reader = new BufferedReader(new FileReader(file));
			int numBars = Integer.parseInt(reader.readLine());
			bars = new Rectangle[numBars];
			for(int i=0;i<numBars;i++){
				bars[i] = new Rectangle(reader.readLine());
			}
			this.ball = new Rectangle(reader.readLine());//writer.println(this.getBall());
			this.state = Integer.parseInt(reader.readLine());//writer.println(this.getState());
			this.vx = Double.parseDouble(reader.readLine());//writer.println(this.getVx());
			this.vy = Double.parseDouble(reader.readLine());//writer.println(this.getVy());
			//writer.println(this.getPlayersReadyOnFinish());
			this.playersReadyOnFinish = new boolean[numBars];
			for(int i=0;i<numBars;i++) {
				this.playersReadyOnFinish[i] = Boolean.parseBoolean(reader.readLine());
			}
			nbLastPlayerThatHitTheBall = Integer.parseInt(reader.readLine());//writer.println(this.getNbLastPlayerThatHitTheBall());
			maxPoints = Integer.parseInt(reader.readLine());//writer.println(this.getMaxPoints());
			this.players = new IPlayer[numBars];
			for (int i = 0; i < numBars; i++) {
				int playerIndex = i;
				int score = 0;
				String info = reader.readLine();
				try {
					playerIndex = Integer.parseInt(info.substring(0, info.indexOf(" ")));
					score = Integer.parseInt(info.substring(info.indexOf(" ")+1));
				}
				catch (NumberFormatException e) {e.printStackTrace();}
				players[i] = new Player(playerIndex);
				players[i].setScore(score);
			}
		} catch(IOException e) {
			System.out.println("yes");
		} finally{
			try {
				reader.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void saveGameState(File file) {
		PrintWriter writer = null;
		try{
			writer = new PrintWriter(file);
			writer.println(this.getBars().length);
			for(Rectangle r : this.getBars()){
				writer.println(r);
			}
			writer.println(this.getBall());
			writer.println(this.getState());
			writer.println(this.getVx());
			writer.println(this.getVy());
			for(boolean b : this.getPlayersReadyOnFinish()){
				writer.println(b);
			}
			writer.println(this.getNbLastPlayerThatHitTheBall());
			writer.println(this.getMaxPoints());
			for (IPlayer iPlayer : this.players) {
				writer.println(iPlayer.getPlayerNumber()+" "+iPlayer.getScore());
			}
		} catch(IOException e) {
			e.printStackTrace();
		}
		finally {
			writer.close();
		}
	}
	
	private void setupNewGame() {
		// BARS
		int nbPlayers = players.length;
		bars = new Rectangle[nbPlayers];
		bars[0] = new Rectangle(10, HEIGHT / 2, 10, 100); //bloque p1
		bars[1] = new Rectangle(WIDTH - 10, HEIGHT / 2, 10, 100); //bloque p2
		if(nbPlayers >= 3){ //creamos bloques para potenciales p3 y p4
			bars[2] = new Rectangle(WIDTH/2, HEIGHT - 10, 100, 10);
		}
		if(nbPlayers == 4){
			bars[3] = new Rectangle(WIDTH/2, 10, 100, 10);
		}
		// BALL
		ball = new Rectangle(WIDTH * 0.5, HEIGHT * 0.5, 10, 10);
		
		nbLastPlayerThatHitTheBall=-1;
		/* resetear la velocidad de la pelota */
		int angulo = (int)(Math.random()*100)-50;
		this.vy = initialSpeed*Math.sin(Math.toRadians(angulo));
		if(Math.random()>0.5)
			this.vx = initialSpeed*Math.cos(Math.toRadians(angulo));
		else
			this.vx = -initialSpeed*Math.cos(Math.toRadians(angulo));
		
	}

	@Override
	public void init() {
		Thread game = new Thread(new Runnable() {

			@Override
			public void run() {
				juego: while (true) {
					try {
						
						if(getState() == S_PAUSED) continue juego;
						
						if (getState() == S_GAMEFINISHED) {
							try {
								for (int i = 0; i < players.length; i++) {
									players[i].setPauseRequested(true);
								}
								boolean allReady = true;
								for (Boolean b : playersReadyOnFinish) {
									allReady &= b;
								}
								if(allReady) {
									for (int i = 0; i < players.length; i++) {
										players[i].setScore(0);
									}
									for(int i=0;i<playersReadyOnFinish.length;i++) {
										playersReadyOnFinish[i] = false;
									}
									setState(S_RUNNING);
								}
							} catch (RemoteException e) {e.printStackTrace();}
							continue juego;
						}
						
						/* mover p1 y p2 */
						
						if (players[0].isUp()) {
							if (bars[0].y - bars[0].h * 0.5 - DX >= 0)
								bars[0].y -= DX;
						}
						if (players[0].isDown()) {
							if (bars[0].y + bars[0].h * 0.5 + DX < HEIGHT)
								bars[0].y += DX;
						}
						if (players[1].isUp()) {
							if (bars[1].y - bars[1].h * 0.5 - DX >= 0)
								bars[1].y -= DX;
						}
						if (players[1].isDown()) {
							if (bars[1].y + bars[1].h * 0.5 + DX < HEIGHT)
								bars[1].y += DX;
						}
						
						/* mover p3 y p4 si existen */
						
						if(players.length >= 3){
							if (players[2].isUp()) {
								if (bars[2].x - bars[2].w * 0.5 - DX >= 0)
									bars[2].x -= DX;
							}
							if (players[2].isDown()) {
								if (bars[2].x + bars[2].w * 0.5 + DX < WIDTH)
									bars[2].x += DX;
							}
						}
						
						if(players.length == 4){
							if (players[3].isUp()) {
								if (bars[3].x - bars[2].w * 0.5 - DX >= 0)
									bars[3].x -= DX;
							}
							if (players[3].isDown()) {
								if (bars[3].x + bars[2].w * 0.5 + DX < WIDTH)
									bars[3].x += DX;
							}
						}
					} catch (RemoteException e) {e.printStackTrace();}
					// actualiza posicion
					ball.x += vx * DX;
					ball.y += vy * DX;

					// rebote en y

					/*rebote con pared superior*/
					if (ball.y - ball.h * 0.5 <= 0 && players.length < 4) vy = -vy;
					
					/*rebote con pared inferior*/
					if (ball.y + ball.h * 0.5 >= HEIGHT && players.length < 3) vy = -vy;
					
					
					/* resetear el juego si la pelota sale del tablero */
					if(ball.getX() < 0 || ball.getX() > WIDTH || ball.getY() < 0 || ball.getY() > HEIGHT) {
						try {
							if(getNbLastPlayerThatHitTheBall() != -1){
								players[getNbLastPlayerThatHitTheBall()].setScore(players[getNbLastPlayerThatHitTheBall()].getScore()+1);
								if(players[getNbLastPlayerThatHitTheBall()].getScore() == maxPoints) {
									setState(S_GAMEFINISHED);
								}
								for (IPlayer p: players) {
									p.setUp(false);
									p.setDown(false);
								}
								//System.out.println(scoresToString());
							}
							resetGame();
						} catch (RemoteException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
					// rebote con paletas

					for (int i = 0; i < bars.length; i++) {
						Rectangle bar = bars[i];
						if (ball.bottom() < bar.top() && ball.top() > bar.bottom()) { // esta dentro
																// en
																// Y
							if ((vx > 0 && ball.left() <= bar.left() && ball.right() >= bar.left()) // esta a la
															// izquierda y se
															// mueve a la
															// derecha
									// o esta a la derecha y se mueve hacia la
									// izquierda
									|| (vx < 0 && ball.right() >= bar.right() && ball
											.left() <= bar.right())) {

								vx = -vx * (1 + DV);
								setNbLastPlayerThatHitTheBall(i);
								break;
							}
						}
						
						if (ball.left() < bar.right() && ball.right() > bar.left()) { // esta dentro
																// en
																// X
							if ((vy > 0 && ball.bottom() <= bar.bottom() && ball.top() >= bar.bottom()) // esta 
															// abajo y se
															// mueve hacia
															// arriba
									// o esta a la arriba y se mueve hacia abajo
									|| (vy < 0 && ball.top() >= bar.top() && ball
											.bottom() <= bar.top())) {

								vy = -vy * (1 + DV);
								setNbLastPlayerThatHitTheBall(i);
								break;
							}
						}
					}
					try {
						Thread.sleep(1000 / UPDATE_RATE); // milliseconds
					} catch (InterruptedException ex) {
					}
					
					for(int i = 0 ; i< players.length ; i++) { 
						try {
							if (players[i].getScore() >= maxPoints) {
								setState(S_GAMEFINISHED);
							}
						} catch (RemoteException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
					
				}
			}

		});
		game.start();
	}
	

	
	@Override
	public Rectangle[] getBars() throws RemoteException {
		return bars;
	}
	@Override
	public Rectangle getBall() throws RemoteException {
		return ball;
	}
	public void resetGame() throws RemoteException{
		setupNewGame();
		setState(S_RUNNING);
	}
	
	public void setNbLastPlayerThatHitTheBall(int i){
		this.nbLastPlayerThatHitTheBall=i;
	}
	public int getNbLastPlayerThatHitTheBall() {
		return nbLastPlayerThatHitTheBall;
	}
	
	@Override
	public List<Integer> getScores() throws RemoteException {
		List<Integer> listaScores = new ArrayList<Integer>();
		for(IPlayer iplayer : players) {
			listaScores.add(iplayer.getScore());
		}
		return listaScores;
	}
	
	@Override
	public void setPlayerReadyOnFinish(int playerNumber) {
		playersReadyOnFinish[playerNumber] = true;
	}
	
	@Override
	public boolean isPlayerReadyOnFinish(int playerNumber) {
		return playersReadyOnFinish[playerNumber];
	}
	
	public void setPlayerSlots(IPlayer[] playerSlots) {
		this.players = playerSlots;
	}

	public boolean[] getPlayersReadyOnFinish() {
		return playersReadyOnFinish;
	}

	public void setPlayersReadyOnFinish(boolean[] playersReadyOnFinish) {
		this.playersReadyOnFinish = playersReadyOnFinish;
	}

	public double getVx() {
		return vx;
	}

	public void setVx(double vx) {
		this.vx = vx;
	}

	public double getVy() {
		return vy;
	}

	public void setVy(double vy) {
		this.vy = vy;
	}
	
	public int getMaxPoints() throws RemoteException {
		return this.maxPoints;
	}
	
	public void getMaxPoints(int maxPoints) throws RemoteException {
		this.maxPoints = maxPoints;
	}
	
	public IPlayer[] getPlayerSlots() throws RemoteException {
		return this.players;
	}
}
