package pongd;

import java.awt.Graphics;
import java.io.Serializable;

public class Rectangle implements Serializable {

	private static final long serialVersionUID = 3490769299828451338L;
	
	double x, y;
	double w, h;

	public Rectangle(double x, double y, double w, double h) {
		this.x = x;
		this.y = y;
		this.w = w;
		this.h = h;
	}

	public Rectangle(String s){
		String[] sa = s.split(" ");
		this.x = Double.parseDouble(sa[0]);
		this.y = Double.parseDouble(sa[1]);
		this.w = Double.parseDouble(sa[2]);
		this.h = Double.parseDouble(sa[3]);
	}
	
	@Override
	public String toString() {
		return x+" "+y+" "+w+" "+h;
	}
	
	
	public void draw(Graphics graphics) {
		graphics.fillRect(left(), bottom(), (int) w, (int) h);
	}

	public int top() {
		return (int) (y + h * 0.5);
	}

	public int left() {
		return (int) (x - w * 0.5);
	}

	public int bottom() {
		return (int) (y - h * 0.5);
	}

	public int right() {
		return (int) (x + w * 0.5);
	}

	public double getY() {
		return y;
	}

	public double getX() {
		return x;
	}

}
