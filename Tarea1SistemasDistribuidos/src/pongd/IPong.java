package pongd;

import java.io.File;
import java.rmi.Remote;
import java.rmi.RemoteException;
import java.util.List;

public interface IPong extends Remote {
	public Rectangle[] getBars() throws RemoteException;
	public Rectangle getBall() throws RemoteException;
	public void init() throws RemoteException;
	public int getState() throws RemoteException;
	public int getErrorState() throws RemoteException;
	public List<Integer> getScores() throws RemoteException;
	public void setPlayerReadyOnFinish(int playerNumber) throws RemoteException;
	public boolean isPlayerReadyOnFinish(int playerNumber) throws RemoteException;
	public void setPlayerSlots(IPlayer[] playerSlots) throws RemoteException;
	public IPlayer[] getPlayerSlots() throws RemoteException;
	public boolean[] getPlayersReadyOnFinish() throws RemoteException;
	public void setPlayersReadyOnFinish(boolean[] playersReadyOnFinish) throws RemoteException;
	public int getNbLastPlayerThatHitTheBall() throws RemoteException;
	public double getVx() throws RemoteException;
	public void setVx(double vx) throws RemoteException;
	public double getVy() throws RemoteException;
	public void setVy(double vy) throws RemoteException;
	public int getMaxPoints() throws RemoteException;
	public void getMaxPoints(int maxPoints) throws RemoteException;
	public IPlayer[] getPlayers() throws RemoteException;
	public void setState(int sPaused) throws RemoteException;
	public void saveGameState(File file) throws RemoteException;
}
