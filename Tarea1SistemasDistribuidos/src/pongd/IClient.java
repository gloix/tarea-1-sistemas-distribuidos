package pongd;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface IClient extends Remote {
	public boolean isAlive() throws RemoteException;
	public void migrate(IPong p, int PlayerNumber) throws RemoteException;
	public void togglePause() throws RemoteException;
}
